import 'dart:io';
import 'package:fruit_shop_app/models/fruits.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'db_product.db');
    return await openDatabase(
      path,
      version: 5,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE productDB (
          id INTEGER PRIMARY KEY,
          fruitName TEXT,
          imagesName TEXT,
          sale TEXT
      )
      ''');
  }

  Future<List<Fruits>> getProduts() async {
    Database db = await instance.database;
    var products = await db.query('productDB', orderBy: 'fruitName');
    List<Fruits> groceryList = products.isNotEmpty
        ? products.map((c) => Fruits.fromMap(c)).toList()
        : [];
    return groceryList;
  }

  Future<List<Fruits>> getProduct(int id) async{
    Database db = await instance.database;
    var product = await db.query('productDB', where: 'id = ?', whereArgs: [id]);
    List<Fruits> profileList = product.isNotEmpty
        ? product.map((c) => Fruits.fromMap(c)).toList()
        : [];
    return profileList;
  }

  Future<int> add(Fruits product) async {
    Database db = await instance.database;
    return await db.insert('productDB', product.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('productDB', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(Fruits product) async {
    Database db = await instance.database;
    return await db.update('productDB', product.toMap(),
        where: "id = ?", whereArgs: [product.id]);
  }
}