import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fruit_shop_app/pages/about_us.dart';
import 'package:fruit_shop_app/pages/add_fruit.dart';
import 'package:fruit_shop_app/pages/allFruits.dart';
import 'package:fruit_shop_app/models/fruits.dart';
import 'package:fruit_shop_app/pages/contact.dart';
import 'package:fruit_shop_app/pages/welcome.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int? selectedId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            ImageSlideshow(
              /// Width of the [ImageSlideshow].
              width: double.infinity,

              /// Height of the [ImageSlideshow].
              height: 200,

              /// The page to show when first creating the [ImageSlideshow].
              initialPage: 0,

              /// The color to paint the indicator.
              indicatorColor: Colors.blue,

              /// The color to paint behind th indicator.
              indicatorBackgroundColor: Colors.grey,

              /// The widgets to display in the [ImageSlideshow].
              /// Add the sample image file into the images folder
              children: [
                Image.asset(
                  'images/1.png',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'images/2.png',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'images/3.png',
                  fit: BoxFit.cover,
                ),
              ],

              /// Called whenever the page in the center of the viewport changes.
              onPageChanged: (value) {
                print('Page changed: $value');
              },

              /// Auto scroll interval.
              /// Do not auto scroll with null or 0.
              autoPlayInterval: 3000,

              /// Loops back to first slide.
              isLoop: true,
            ),
            Padding(
              padding: const EdgeInsets.only(),
              child: ListTile(
                leading: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(FontAwesomeIcons.cartShopping,
                      color: Color(0xFFF87207)),
                ),
                tileColor: Color(0xFFACFA7D),
                title: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'สินค้าทั้งหมด',
                    style: TextStyle(
                        fontFamily: "IBM",
                        fontSize: 16,
                        color: Color(0xFF000B2F)),
                  ),
                ),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return AllFruits(
                      title: 'All Fruits',
                    );
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                leading: Icon(
                  Icons.add_circle,
                  color: Colors.pink,
                  size: 30,
                ),
                title: Text(
                  'เพิ่มสินค้า',
                  style: TextStyle(fontFamily: "IBM", fontSize: 16),
                ),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return AddFruit();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                leading: Icon(FontAwesomeIcons.doorOpen, color: Colors.blue),
                title: Text(
                  'กลับสู่หน้าหลัก',
                  style: TextStyle(fontFamily: "IBM", fontSize: 16),
                ),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return WelcomePage();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                leading: Icon(
                  FontAwesomeIcons.phone,
                  color: Colors.green,
                ),
                title: Text('ติดต่อเรา',
                    style: TextStyle(fontFamily: "IBM", fontSize: 16)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return Contact();
                  }
                  )
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                leading: Icon(FontAwesomeIcons.circleInfo),
                title: Text('เกี่ยวกับเรา',
                    style: TextStyle(fontFamily: "IBM", fontSize: 16)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return AboutUs();
                  }));
                },
              ),
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: const Color(0xFF1E1E1E),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 33.0, left: 20.0, right: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Builder(builder: (context) {
                  return IconButton(
                      icon: const Icon(
                        Icons.menu,
                        size: 33.0,
                        color: Colors.white,
                      ),
                      tooltip: "Menu",
                      onPressed: () {
                        Scaffold.of(context).openDrawer();
                      });
                }),
                Row(
                  children: [
                    Image.asset('images/logofs.png', height: 50),
                  ],
                ),
              ],
            ),
          ),
          Center(
            child: Padding(
              padding:
                  const EdgeInsets.only(top: 90.0, left: 20.0, right: 20.0),
              child: Column(
                children: [
                  ImageSlideshow(
                    /// Width of the [ImageSlideshow].
                    width: double.infinity,

                    /// Height of the [ImageSlideshow].
                    height: 220,

                    /// The page to show when first creating the [ImageSlideshow].
                    initialPage: 0,

                    /// The color to paint the indicator.
                    indicatorColor: Colors.blue,

                    /// The color to paint behind th indicator.
                    indicatorBackgroundColor: Colors.grey,

                    /// The widgets to display in the [ImageSlideshow].
                    /// Add the sample image file into the images folder
                    children: [
                      Image.asset(
                        'images/pr1.png',
                        fit: BoxFit.cover,
                      ),
                      Image.asset(
                        'images/pr2.png',
                        fit: BoxFit.cover,
                      ),
                      Image.asset(
                        'images/pr3.png',
                        fit: BoxFit.cover,
                      ),
                    ],

                    /// Called whenever the page in the center of the viewport changes.
                    onPageChanged: (value) {
                      print('Page changed: $value');
                    },

                    /// Auto scroll interval.
                    /// Do not auto scroll with null or 0.
                    autoPlayInterval: 3000,

                    /// Loops back to first slide.
                    isLoop: true,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 330, left: 39),
            child: SizedBox(
              width: 320,
              height: 60,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AllFruits(
                                title: 'All Fruits',
                              )));
                },
                style: ElevatedButton.styleFrom(
                    primary: const Color(0xFFF1C950),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                    )),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    Text(
                      "สินค้าของฉัน",
                      style: TextStyle(
                          color: Color(0xFF000000),
                          fontSize: 22.0,
                          fontFamily: "IBM",
                          fontWeight: FontWeight.w700,
                          height: 2),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      // <-- Icon
                      Icons.shopping_cart,
                      size: 24.0,
                      color: Color(0xFF000000),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 73.0, left: 30.0, right: 20.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "สินค้าขายดีที่สุด",
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "IBM",
                        fontWeight: FontWeight.w700,
                        fontSize: 20.0,
                        height: 2),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AllFruits(
                                    title: 'All Fruits',
                                  )));
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const [
                        Text(
                          "ดูทั้งหมด",
                          style: TextStyle(
                              color: Color(0xFFF1C950),
                              fontFamily: "IBM",
                              fontWeight: FontWeight.normal,
                              height: 2),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          Icons.arrow_forward,
                          color: Color(0xFFF1C950),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Center(
            child: Padding(
              padding:
                  const EdgeInsets.only(top: 430.0, left: 20.0, right: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: 175,
                        height: 300,
                        child: Card(
                          color: const Color(0xFF2C2C2C),
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(75.0),
                                  topRight: Radius.circular(75.0),
                                  bottomLeft: Radius.circular(30.0),
                                  bottomRight: Radius.circular(30.0))),
                          child: Padding(
                            padding: const EdgeInsets.only(top: 25.0),
                            child: Column(
                              children: [
                                SizedBox(
                                  width: 110,
                                  height: 110,
                                  child: Image.asset("images/fruits/apple.png"),
                                ),
                                const Padding(
                                  padding:
                                      EdgeInsets.only(top: 20.0, left: 15.0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Fruit",
                                      style: TextStyle(
                                        color: Color(0xFFF1C950),
                                        fontFamily: "Inter",
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal,
                                        letterSpacing: 5.0,
                                      ),
                                    ),
                                  ),
                                ),
                                const Padding(
                                  padding:
                                      EdgeInsets.only(top: 10.0, left: 15.0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Apple",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 25,
                                          fontFamily: "Inter",
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 15.0, left: 15.0),
                                  child: Row(
                                    children: const [
                                      Text(
                                        "ขายดีอันดับ 1",
                                        style: TextStyle(
                                          color: Color(0xFF85E845),
                                          fontSize: 18,
                                          fontFamily: "IBM",
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 175,
                        height: 300,
                        child: Card(
                          color: const Color(0xFF2C2C2C),
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(75.0),
                                  topRight: Radius.circular(75.0),
                                  bottomLeft: Radius.circular(30.0),
                                  bottomRight: Radius.circular(30.0))),
                          child: Padding(
                            padding: const EdgeInsets.only(top: 25.0),
                            child: Column(
                              children: [
                                SizedBox(
                                  width: 110,
                                  height: 110,
                                  child:
                                      Image.asset("images/fruits/banana.png"),
                                ),
                                const Padding(
                                  padding:
                                      EdgeInsets.only(top: 20.0, left: 15.0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Fruit",
                                      style: TextStyle(
                                        color: Color(0xFFF1C950),
                                        fontFamily: "Inter",
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal,
                                        letterSpacing: 5.0,
                                      ),
                                    ),
                                  ),
                                ),
                                const Padding(
                                  padding:
                                      EdgeInsets.only(top: 10.0, left: 15.0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Banana",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 25,
                                          fontFamily: "Inter",
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 15.0, left: 15.0),
                                  child: Row(
                                    children: const [
                                      Text(
                                        "ขายดีอันดับ 2",
                                        style: TextStyle(
                                          color: Color(0xFF85E845),
                                          fontSize: 18,
                                          fontFamily: "IBM",
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
