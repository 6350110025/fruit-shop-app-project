class Fruits {
  int? id;
  String fruitName;
  String imagesName;
  String sale;

  Fruits(
      {this.id,
      required this.fruitName,
      required this.imagesName,
      required this.sale});

  factory Fruits.fromMap(Map<String, dynamic> json) => new Fruits(
      id: json['id'],
      fruitName: json['fruitName'],
      imagesName: json['imagesName'],
      sale: json['sale']);

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'fruitName': fruitName,
      'imagesName': imagesName,
      'sale': sale,
    };
  }
}
