import 'package:flutter/material.dart';
import 'package:fruit_shop_app/MainScreen.dart';
import 'package:fruit_shop_app/pages/contact.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);






  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: const Color(0xFF2C2C2C),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 300,
                    child: Image.asset("images/vegetablesFruit.png"),
                  ),
                  const SizedBox(
                    height: 400,
                  )
                ],
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  SizedBox(
                    height: 105,
                  ),
                  Text("KEEPING YOU HEALTHY",
                    style: TextStyle(
                      fontSize: 12.0,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.normal,
                      color: Color(0xFFACFA7D),
                      letterSpacing: 7.0,
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text("สุขภาพดีเริ่มต้น\n ที่ตัวคุณ",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: "IBM",
                      fontSize: 45.0,
                      letterSpacing: 1.0,
                    ),
                  ),

                  SizedBox(
                    height: 10.0,
                  ),
                  Text("ผลไม้สดใหม่ ปลอดภัย ไร้สารพิษ\n"
                      "ส่งตรงถึงมือคุณด้วยปลายนิ้วโดย\n"
                      "Fruit Shop Delivery App.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white30,
                      fontWeight: FontWeight.bold,
                      fontFamily: "IBM",
                      fontSize: 15.0,
                      letterSpacing: 0.5,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 90.0),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: TextButton(
                  child: const Text("พบปัญหาการใช้งานโปรดติดต่อเราที่นี่",
                    style:TextStyle(
                        color: Color(0xFFACFA7D),
                        fontSize: 12.0,
                        fontFamily: "IBM",
                        fontWeight: FontWeight.w700
                    ),
                  ),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return Contact();
                    }
                    )
                    );
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 30.0),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: SizedBox(
                  width: 320,
                  height: 60,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const MainScreen()));
                    },
                    style: ElevatedButton.styleFrom(
                        primary: const Color(0xFF1FE80B),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        )
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const [
                        Text("ขายกับเราตอนนี้",
                          style: TextStyle(
                            color: Color(0xFF000000),
                            fontSize: 18.5,
                            fontFamily: "IBM",
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Icon( // <-- Icon
                          Icons.arrow_forward_ios,
                          size: 18.0,
                          color: Color(0xFF2C2C2C),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),

    );
  }
}
