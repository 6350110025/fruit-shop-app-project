import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fruit_shop_app/models/fruits.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:developer' as developer;

import '../db/database_helper.dart';

class EditFruit extends StatefulWidget {
  Fruits model;

  EditFruit(this.model);

  @override
  _EditFruitState createState() => _EditFruitState(model);
}

class _EditFruitState extends State<EditFruit> {
  var fruitName = TextEditingController();
  var sale = TextEditingController();

  Fruits model;
  int? selectedId;

  var _image;
  var imagePicker;
  _EditFruitState(this.model);

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
    this.selectedId = this.model.id;
    fruitName  = TextEditingController()..text = this.model.fruitName;
    sale = TextEditingController()..text = this.model.sale;
    _image = this.model.imagesName;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Fruit Products'),
        backgroundColor: Color(0xFF1E1E1E),
      ),
      body: Container(
        color: Color(0xFFEEEEEE),
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.15),
                      child: GestureDetector(
                        onTap: () async {
                         final image = await imagePicker.pickImage(
                              source: ImageSource.gallery,
                              imageQuality: 50,
                              preferredCameraDevice: CameraDevice.front);

                          setState(() {
                            _image = image.path;
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration:
                              BoxDecoration(color: Color(0xFF6F3F3)),
                          child: _image != null
                              ? Image.file(
                                  File(_image),
                                  width: 200.0,
                                  height: 200.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Colors.black45),
                                  width: 200,
                                  height: 200,
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(15),
                            child: TextField(
                              style: TextStyle(color: Color(0xFF000726)),
                              controller: fruitName,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Fruit Name',
                                hintText: 'Enter Fruit Name',
                                labelStyle: TextStyle(color: Color(0xFF000000)),
                                hintStyle: TextStyle(color: Color(0xFF696767)),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(15),
                            child: TextField(
                              style: TextStyle(color: Color(0xFF000726)),
                              controller: sale,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Price',
                                hintText: 'Enter Fruit Price',
                                labelStyle: TextStyle(color: Color(0xFF000000)),
                                hintStyle: TextStyle(color: Color(0xFF696767)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('Update'),
                          buildElevatedButton('Cancel'),
                        ],
                      ),
                    ),
                  ],

                ),

              ),
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton buildElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () async {
        if (title == 'Update') {
              await DatabaseHelper.instance.update(
                  Fruits(
                      id: selectedId,
                      fruitName: fruitName.text,
                      sale: sale.text,
                      imagesName: _image,),
                );
          setState(() {
            //firstname.clear();
            selectedId = null;
          });
          Navigator.pop(context);
        }else Navigator.pop(context);
      }
      ,
      style: ElevatedButton.styleFrom(
          fixedSize: Size(120, 50),
          primary: Color(0xFF1F1F1F),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          textStyle:
              const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      child: Text(
        title,
      ),
    );
  }

  Padding buildTextfield(String title, final ctrl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: TextField(
        controller: ctrl,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: title,
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }

}
