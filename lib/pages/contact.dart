import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Contact extends StatefulWidget {
  Contact({
    Key? key,
  }) : super(key: key);

  @override
  State<Contact> createState() => _ContactState();
}

class _ContactState extends State<Contact> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF1E1E1E),
        title: Text('Contact Us'),
      ),
      backgroundColor: const Color(0xFF1E1E1E),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 0.0, left: 20.0, right: 20.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 7),
                child: Center(
                  child: Column(
                    children: [
                      Image.asset(
                        'images/logofull-removebg-preview.png',
                        height: 250,
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 35,
                ),
                child: Text(
                  'Fruit Shop Delivery Inc.',
                  style: TextStyle(
                      fontSize: 30,
                      color: Color(0xFFF1C950),
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 15,
                  left: 4,
                  right: 6,
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.phone,size: 25,color: Colors.white,),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '044-444-444',
                            style: TextStyle(
                                fontFamily: 'IBM', fontSize: 18, color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.email,size: 25,color: Colors.white,),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'support@fruitshop.co.th',
                            style: TextStyle(
                                fontFamily: 'IBM', fontSize: 18, color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(FontAwesomeIcons.earthAsia,color: Colors.white,),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'www.fruitshop.co.th/delivery',
                            style: TextStyle(
                                fontFamily: 'IBM', fontSize: 18, color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
