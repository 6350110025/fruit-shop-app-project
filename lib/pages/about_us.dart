import 'dart:io';

import 'package:flutter/material.dart';

class AboutUs extends StatefulWidget {
  AboutUs({
    Key? key,
  }) : super(key: key);

  @override
  State<AboutUs> createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF1E1E1E),
        title: Text('About Us'),
      ),
      backgroundColor: const Color(0xFF1E1E1E),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 0.0, left: 20.0, right: 20.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 7),
                child: Center(
                  child: Column(
                    children: [
                      Image.asset(
                        'images/logofull-removebg-preview.png',
                        height: 250,
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 35,
                ),
                child: Text(
                  'Fruit Shop App',
                  style: TextStyle(
                      fontSize: 30,
                      color: Color(0xFFF1C950),
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 15,
                  left: 4,
                  right: 6,
                ),
                child: Text(
                  '    แอปพลิเคชันนี้สร้างขึ้นเพื่อให้บริการซื้อสินค้าประเภทผลไม้ด้วย Fruit Shop App เพื่ออำนวยความสะดวกให้กับผู้ซื้อและผู้ขายของ Fruit Shop App\n\n   ผู้ซื้อและผู้ขายสามารถใช้งาน Fruit Shop App ได้ฟรีและแอปพลิเคชันเป็นเพียงแค่การทดสอบและมิได้นำไปใช้จริงแต่อย่างใด',
                  style: TextStyle(
                      fontFamily: 'IBM', fontSize: 15, color: Colors.white),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(top: 80, left: 7),
                child: Center(
                  child: Column(
                    children: [
                      Image.asset(
                        'images/aboutlogo.png',
                        height: 250,
                      ),
                    ],
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(
                  top: 45,
                ),
                child: Text(
                  'Developers',
                  style: TextStyle(
                      fontSize: 30,
                      color: Color(0xFFF1C950),
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 15,
                  left: 4,
                  right: 6,
                ),
                child: Text(
                  'ผู้สร้างและพัฒนาแอป Fruit Shop App คือส่วนสำคัญ',
                  style: TextStyle(
                      fontFamily: 'IBM', fontSize: 15, color: Colors.white),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20,bottom: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 175,
                      height: 300,
                      child: Card(
                        color: const Color(0xFF2C2C2C),
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(75.0),
                                topRight: Radius.circular(75.0),
                                bottomLeft: Radius.circular(30.0),
                                bottomRight: Radius.circular(30.0))),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 25.0),
                          child: Column(
                            children: [
                              SizedBox(
                                width: 110,
                                height: 110,
                                child: CircleAvatar(
                                  backgroundImage: AssetImage('images/profile1.png'),
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.only(top: 20.0, left: 15.0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "6350110025",
                                    style: TextStyle(
                                      color: Color(0xFFF1C950),
                                      fontFamily: "Inter",
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal,
                                      letterSpacing: 4.0,
                                    ),
                                  ),
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.only(top: 10.0, left: 15.0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "สืบสกุล ค.",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 25,
                                        fontFamily: "IBM",
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 15.0),
                                child: Row(
                                  children: const [
                                    Text(
                                      "นักพัฒนาระบบ",
                                      style: TextStyle(
                                        color: Color(0xFF9EFADD),
                                        fontSize: 18,
                                        fontFamily: "IBM",
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 175,
                      height: 300,
                      child: Card(
                        color: const Color(0xFF2C2C2C),
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(75.0),
                                topRight: Radius.circular(75.0),
                                bottomLeft: Radius.circular(30.0),
                                bottomRight: Radius.circular(30.0))),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 25.0),
                          child: Column(
                            children: [
                              SizedBox(
                                width: 110,
                                height: 110,
                                child: CircleAvatar(
                                  backgroundImage: AssetImage('images/profilephet.jpg'),
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.only(top: 20.0, left: 15.0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "6350110019",
                                    style: TextStyle(
                                      color: Color(0xFFF1C950),
                                      fontFamily: "Inter",
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal,
                                      letterSpacing: 4.0,
                                    ),
                                  ),
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.only(top: 10.0, left: 15.0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "ศุภณัฐ อ.",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 25,
                                        fontFamily: "IBM",
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, left: 15.0),
                                child: Row(
                                  children: const [
                                    Text(
                                      "นักพัฒนาระบบ",
                                      style: TextStyle(
                                        color: Color(0xFF9EFADD),
                                        fontSize: 18,
                                        fontFamily: "IBM",
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
