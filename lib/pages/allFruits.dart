import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fruit_shop_app/db/database_helper.dart';
import 'package:fruit_shop_app/models/fruits.dart';
import 'package:fruit_shop_app/pages/add_fruit.dart';
import 'package:fruit_shop_app/pages/edit_fruit.dart';
import 'package:fruit_shop_app/pages/show_fruit.dart';

class AllFruits extends StatefulWidget {
  AllFruits({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  State<AllFruits> createState() => _AllFruitsState();
}

class _AllFruitsState extends State<AllFruits> {
  int? selectedId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF1E1E1E),
        title: Text(widget.title),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: IconButton(
              icon: const Icon(Icons.add_circle),
              tooltip: 'Add Fruits',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddFruit()),
                ).then((value) {
                  //getAllData();
                  setState(() {});
                });
              },
            ),
          ),
        ],
      ),
      backgroundColor: const Color(0xFF1E1E1E),
      body: Center(
        child: FutureBuilder<List<Fruits>>(
            future: DatabaseHelper.instance.getProduts(),
            builder:
                (BuildContext context, AsyncSnapshot<List<Fruits>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('กำลังโหลดข้อมูล...',
                  style: TextStyle(
                      fontFamily: 'IBM',
                      fontSize: 16,
                      color: Color(0xFFF5ED7D)),));
              }
              return snapshot.data!.isEmpty
                  ? Center(
                      child: Text(
                      'ยังไม่มีสินค้า กรุณาเพิ่มสินค้าที่ (+)',
                      style: TextStyle(
                          fontFamily: 'IBM',
                          fontSize: 16,
                          color: Color(0xFFF8B0B0)),
                    ))
                  : Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: GridView(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 2 / 3.7,
                          mainAxisSpacing: 4,
                        ),
                        children: snapshot.data!.map((grocery) {
                          return Center(
                            child: GestureDetector(
                              child: Card(
                                color: const Color(0xFF2C2C2C),
                                shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(100.0),
                                        topRight: Radius.circular(100.0),
                                        bottomLeft: Radius.circular(50.0),
                                        bottomRight: Radius.circular(50.0))),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 35.0),
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        width: 150,
                                        height: 150,
                                        child: Image.file(
                                            File(grocery.imagesName)),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 20.0, left: 15.0),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            "Fruit",
                                            style: TextStyle(
                                              color: Color(0xFFF1C950),
                                              fontFamily: "Inter",
                                              fontSize: 15,
                                              fontWeight: FontWeight.normal,
                                              letterSpacing: 5.0,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 5.0, left: 15.0),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            '${grocery.fruitName}',
                                            style: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 30,
                                                fontFamily: "Inter",
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 10.0, left: 15.0),
                                        child: Row(
                                          children: [
                                            Text(
                                              "\฿${grocery.sale}",
                                              style: const TextStyle(
                                                  color: Color(0xFF85E845),
                                                  fontSize: 30,
                                                  fontFamily: "Inter",
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            const Padding(
                                              padding:
                                                  EdgeInsets.only(left: 5.0),
                                              child: Text(
                                                "/ Kg.",
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    color: Colors.grey),
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              onTap: () {
                                var profileid = grocery.id;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ShowFruit(
                                              id: profileid,
                                            )));

                                setState(() {
                                  print(grocery.imagesName);
                                  if (selectedId == null) {
                                    //firstname.text = grocery.firstname;
                                    selectedId = grocery.id;
                                  } else {
                                    // textController.text = '';
                                    selectedId = null;
                                  }
                                });
                              },
                              onLongPress: () {
                                setState(() {
                                  DatabaseHelper.instance.remove(grocery.id!);
                                });
                              },
                            ),
                          );
                        }).toList(),
                      ),
                    );
            }),
      ),
    );
  }
}
