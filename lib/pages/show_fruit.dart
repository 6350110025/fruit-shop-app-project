
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fruit_shop_app/db/database_helper.dart';
import 'package:fruit_shop_app/models/fruits.dart';
import 'package:fruit_shop_app/pages/allFruits.dart';
import 'package:fruit_shop_app/pages/edit_fruit.dart';


class ShowFruit extends StatefulWidget {
  final id;

  ShowFruit({Key? key, required this.id,}) : super(key: key);



  @override
  State<ShowFruit> createState() => _ShowFruitState();
}

class _ShowFruitState extends State<ShowFruit> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder<List<Fruits>>(
            future: DatabaseHelper.instance.getProduct(this.widget.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<Fruits>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Groceries in List.'))
                  : Stack(
                children: snapshot.data!.map((grocery) {
                  return Stack(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        color: const Color(0xFF1E1E1E),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 33.0,left: 20.0,right: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                              icon: const Icon(Icons.arrow_back_outlined,size: 30.0,color: Colors.white,),
                              tooltip: "Back",
                              onPressed: (){
                                Navigator.pop(context);
                              },
                            ),
                            Row(
                              children: [
                                IconButton(
                                  icon:const FaIcon(FontAwesomeIcons.penToSquare,size:25.0,color: Color(
                                      0xFFB6B4B4)),
                                  tooltip: "Cart",
                                  onPressed: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => EditFruit(grocery)),
                                    ).then((value){
                                      setState(() {
                                      });
                                    });

                                  },
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 80.0),
                          child: Column(
                            children:  [
                              Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Text('${grocery.fruitName}',
                                  style: const TextStyle(
                                    fontSize: 40,
                                    fontFamily: "Inter",
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),

                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 100.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                  width: 200,
                                  child: Image.file(File(grocery.imagesName))
                              ),
                            ],
                          ),
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 200.0,left: 20.0,right: 20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("\฿${grocery.sale}",
                                style: const TextStyle(
                                  fontSize: 40,
                                  fontFamily: "Inter",
                                  fontWeight: FontWeight.w700,
                                  letterSpacing: 5.0,
                                  color: Color(0xFFF1C950),
                                ),
                              ),
                              IconButton(
                                icon: const Icon(Icons.check_circle,color: Colors.green,size: 30.0,),
                                onPressed:() {
                                },
                              )
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 300.0,left: 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text("PER KG.",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "Inter",
                                    fontWeight: FontWeight.w700,
                                    letterSpacing: 10,
                                    fontSize: 25.0
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 30.0),
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: SizedBox(
                            width: 320,
                            height: 60,
                            child: ElevatedButton(
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      backgroundColor: Color(0xFF1E1E1E),
                                      title: new Text("คุณต้องการลบสินค้านี้หรือไม่?",style: TextStyle(fontFamily: 'IBM',fontSize: 18,color: Colors.white),),
                                      // content: new Text("Please Confirm"),
                                      actions: [
                                        new TextButton(
                                          onPressed: () {
                                            DatabaseHelper.instance.remove(grocery.id!);
                                            setState(() {
                                              Navigator.push(context, MaterialPageRoute(builder: (context)=> AllFruits(title: 'All Fruits',)));
                                            });
                                          },
                                          child: new Text("OK"),
                                        ),
                                        Visibility(
                                          visible: true,
                                          child: new TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: new Text("Cancel"),
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: const Color(0xFFD03944),
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                  )
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: const [
                                  Text("ลบสินค้านี้",
                                    style: TextStyle(
                                      color: Color(0xFFE0E0E0),
                                      fontSize: 20.0,
                                      fontFamily: "IBM",
                                      fontWeight: FontWeight.w700,
                                      height: 2
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Icon( // <-- Icon
                                    Icons.delete_forever,
                                    size: 24.0,
                                    color: Color(0xFFE0E0E0),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}
